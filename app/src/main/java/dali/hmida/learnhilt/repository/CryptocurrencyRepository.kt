package dali.hmida.learnhilt.repository

import dali.hmida.learnhilt.model.Cryptocurrency

interface CryptocurrencyRepository {

    fun getCryptoCurrency(): List<Cryptocurrency>

}